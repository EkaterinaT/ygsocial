var conditionName = false;
var conditionEmail = false;
var conditionMessage = false;
                
$(document).ready(function () {
    var name = $('#name');
    name.blur(function () {
        if (name.val().length > 1){
            $('#valid-1').text('');
            name.addClass('error');
            name.removeClass('error').addClass('ok');
            conditionName = true;
        }else{
            if(name.val() != ''){
                $('#valid-1').text('Имя дожно быть длиннее 1-го символа');
                $('.span-valid-1').addClass('valid-fade-down fadeInDownSm');
            }else{
                $('#valid-1').text('Вы не ввели имя');
                $('.span-valid-1').addClass('valid-fade-down fadeInDownSm');
            };
            name.addClass('error');
            conditionName = false;
        }
    });
});

$(document).ready(function () {
    var pattern = /^[a-z0-9_-\.]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
    var email = $('#email');
    
    email.blur(function () {
        if (email.val() != ''){
            if(email.val().search(pattern) == 0) {
                $('#valid-2').text('');
    //			  $('#submit').attr('disabled', false);
                    email.removeClass('error').addClass('ok');
                    conditionEmail = true;
                
                }else{
                    $('#valid-2').text('Не подходит');
    //				$('#submit').attr('disabled', true);
                    email.addClass('error');
                    $('.span-valid-2').addClass('valid-fade-down fadeInDownSm');
                    conditionEmail = false;
                };
            }else{
                $('#valid-2').text('Поле e-mail не должно быть пустым!');
                email.addClass('error');
//                   $('#submit').attr('disabled', true);
                $('.span-valid-2').addClass('valid-fade-down fadeInDownSm');
                conditionEmail = false;
            }
    });
});
$(document).ready(function(){
    var message = $('#message');
    
    message.blur(function(){
        if(message.val() != ''){
            if(message.val(). search('<') == -1){
                $('#valid-3').text('');
                message.removeClass('error').addClass('ok');
                conditionMessage = true;
            }else{
                $('#valid-3').text('Не должно быть тегов');                       
                message.addClass('error');
                $('.span-valid-3').addClass('valid-fade-down fadeInDownSm');
                conditionMessage = false; 
            };
        }else{
            $('#valid-3').text('Поле не заполнено');
            message.addClass('error');
            $('.span-valid-3').addClass('valid-fade-down fadeInDownSm');
            conditionMessage = false;
        };
    });
});
$(document).ready(function clearForm(){
    $('.contact-submit').click(function(){
        $('form input[type="text"], form input[type="email"], form textarea').val('');
        $('.alert').addClass('alert-active');
    });
});

//$(document).ready(function (){
//    if ((conditionMessage == true) && (conditionEmail == true) && (conditionName == true)){
//    $('#contact-submit').attr('disabled', false); 
//    };
//});

//$(document).getElementById("contact-submit").addEventListener("click", function(event){
//    if((conditionMessage == true) && (conditionEmail == true) && (conditionName == true)){
//    $('#valid-4').text('Успех!');
//    message.addClass('ok');
//    $('.span-valid-4').addClass('valid-fade-down fadeInDownSm');
//    }else{
//    event.preventDefault();
//    $('#valid-4').text('Не успех!');
//    message.addClass('error');
//    $('.span-valid-4').addClass('valid-fade-down fadeInDownSm');
//    }
//});